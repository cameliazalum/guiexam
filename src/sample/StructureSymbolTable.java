package sample;

public class StructureSymbolTable {
    String variable;
    String valueS;
    StructureSymbolTable (String s, String v) {
        variable = s;
        valueS = v;
    }
    public String getVariable() {
        return variable;
    }

    public void setVariable(String firstName) {
        this.variable = firstName;
    }

    public String getValueS() {
        return valueS;
    }

    public void setValueS(String lastName) {
        this.valueS = lastName;
    }
}
