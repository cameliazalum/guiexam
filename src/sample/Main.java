package sample;

import com.sun.javaws.IconUtil;
import com.sun.jnlp.ApiDialog;
import examsupport.src.Model.Command.Command;
import examsupport.src.Model.Command.ExitCommand;
import examsupport.src.Model.Command.RunExample;
import examsupport.src.Model.Heap.IHeap;
import examsupport.src.Model.Statement.ProgramState;
import examsupport.src.Model.Value.VALUE;
import javafx.application.Application;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import examsupport.src.View.*;
import javafx.util.Callback;
import org.omg.CORBA.WStringValueHelper;

import java.util.*;

public class Main extends Application {

    Stage window;
    ListView<String> listView;
    Button execute;
    public static int numberOfProgramStates = 0;
    static TableView table;
    Button show;
    static TableView symbolTable;
    static ListView exeStack;
    Button runOneStep;
    int pressed = 0;
    static ListView ouList;

    public static examsupport.src.Model.Heap.IHeap heap = null;
    public static examsupport.src.Model.IList out = null;
    public static examsupport.src.Model.File.FileTable files = null;
    public static examsupport.src.Model.IList ids = null;
    public static Map <Integer, examsupport.src.Model.IDictionary> symbolTables = new HashMap<>();
    public static Map<Integer, examsupport.src.Model.IExecStack> execStackMap = new HashMap<>();
    public static List<List<ProgramState>> states = new ArrayList<List<ProgramState>>();

    public static void repopulateHeapTable()
    {
        table.getItems().clear();
        Iterator i = heap.getKeys().iterator();
        Iterator v = heap.getValues().iterator();
        while (i.hasNext() && v.hasNext()) {
            table.getItems().add(new Structure(i.next().toString(), v.next().toString()));
        }
    }

    public static void repopulateOutList()
    {
        ouList.getItems().clear();
        for (int oi = 0; oi <out.size(); oi++)
        {
            String line = out.get(oi).toString();
            ouList.getItems().addAll(line);
        }
    }

    public static void repopulateSymbolTable(examsupport.src.Model.IDictionary symT)
    {

        symbolTable.getItems().clear();
        Iterator si = symT.getKeys().iterator();

        while (si.hasNext()) {
            String s = si.next().toString();
            symbolTable.getItems().add(new StructureSymbolTable(s, symT.get(s).toString()));

        }
    }

    public static void repopulateExeStackList(examsupport.src.Model.IExecStack exe)
    {
        exeStack.setVisible(true);
        exeStack.getItems().clear();
        String [] steps = exe.toString().split(";", -1);

        exeStack.getItems().addAll(steps);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        window = primaryStage;
        listView = new ListView<>();
        table = new TableView();
        table.setEditable(false);
        execute = new Button("Execute");


        String program1 = examsupport.src.View.Main.prg1().toString();
        String program2 = examsupport.src.View.Main.prg2().toString();
        String program3 = examsupport.src.View.Main.prg3().toString();
        String program4 = examsupport.src.View.Main.prg4().toString();
        String program5 = examsupport.src.View.Main.prg5().toString();
        String program6 = examsupport.src.View.Main.prg6().toString();


        listView.getItems().addAll("exit", program1, program2, program3, program4, program5, program6);

        VBox layout = new VBox(6);
        layout.setPadding(new Insets(20));
        execute.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                states.clear();
                symbolTables = new HashMap<>();
                examsupport.src.View.Main.main2();
                table = new TableView();
                ids = new examsupport.src.Model.outputList<>();
                String selectedItem = listView.getSelectionModel().getSelectedItem();
                int index = listView.getSelectionModel().getSelectedIndex();
                Command re = null;
                pressed = 0;
                switch (index){
                    case 0:
                        re = new ExitCommand("0", "Exit");
                        break;
                    case 1:
                        re = new RunExample("1", selectedItem, examsupport.src.View.Main.controller1);

//                        numberOfProgramStates = examsupport.src.View.Main.controller1.repo.getPrgList().size();
                        break;
                    case 2:
                        re = new RunExample("2", selectedItem, examsupport.src.View.Main.controller2);
//                        numberOfProgramStates = examsupport.src.View.Main.controller2.repo.getPrgList().size();
                        break;
                    case 3:
                        re = new RunExample("3", selectedItem, examsupport.src.View.Main.controller3);
//                        numberOfProgramStates = examsupport.src.View.Main.controller3.repo.getPrgList().size();
                        break;
                    case 4:
                        re = new RunExample("4", selectedItem, examsupport.src.View.Main.controller4);
//                        numberOfProgramStates = examsupport.src.View.Main.controller4.repo.getPrgList().size();
                        break;
                    case 5:
                        re = new RunExample("5", selectedItem, examsupport.src.View.Main.controller5);
//                        numberOfProgramStates = examsupport.src.View.Main.controller5.repo.getPrgList().size();
                        break;
                    case 6:
                        re = new RunExample("6", selectedItem, examsupport.src.View.Main.controller6);

//                        numberOfProgramStates = examsupport.src.View.Main.controller6.repo.getPrgList().size();
                        break;
                }

                re.execute();
                TableColumn address = new TableColumn("address");
                TableColumn value = new TableColumn("value");

                address.setCellValueFactory(new PropertyValueFactory<>("address"));
                value.setCellValueFactory(new PropertyValueFactory<>("value"));

                table.getColumns().setAll(address, value);

                Iterator i = heap.getKeys().iterator();
                Iterator v = heap.getValues().iterator();
                while (i.hasNext() && v.hasNext()) {
                    table.getItems().add(new Structure(i.next().toString(), v.next().toString()));
                }

                ouList = new ListView();
                
                for (int oi = 0; oi <out.size(); oi++)
                {
                    String line = out.get(oi).toString();
                    ouList.getItems().addAll(line);
                }
                ListView fileList = new ListView();

                for (int fi = 0; fi < files.size(); fi ++) {
                    String line = files.get(fi).toString();
                    fileList.getItems().addAll(line);
                }

                ListView idList = new ListView();

                for (int li = 0; li < ids.size(); li ++) {
                    String line = ids.get(li).toString();
                    idList.getItems().addAll(line);
                }

                show = new Button("Show");
                show.setLayoutX(500);
                show.setLayoutY(500);

                symbolTable = new TableView();
                TableColumn variable = new TableColumn("variable");
                TableColumn valueS = new TableColumn("valueS");
                variable.setCellValueFactory(new PropertyValueFactory<>("variable"));
                valueS.setCellValueFactory(new PropertyValueFactory<>("valueS"));
                symbolTable.getColumns().addAll(variable, valueS);
                exeStack = new ListView();
                exeStack.setVisible(false);

                show.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        symbolTable.getItems().clear();
                        exeStack.getItems().clear();
                        examsupport.src.Model.IDictionary symT = symbolTables.get(Integer.parseInt(idList.getSelectionModel().getSelectedItem().toString()));
//                        System.out.println(symT);
                        Iterator si = symT.getKeys().iterator();

                        while (si.hasNext()) {
                            String s = si.next().toString();
                            symbolTable.getItems().add(new StructureSymbolTable(s, symT.get(s).toString()));

                        }
                        exeStack.setVisible(true);
                        examsupport.src.Model.IExecStack exe = execStackMap.get(Integer.parseInt(idList.getSelectionModel().getSelectedItem().toString()));
                        String [] steps = exe.toString().split(";", -1);

                        exeStack.getItems().addAll(steps);
                    }
                });
                runOneStep = new Button("Run One Step");

                runOneStep.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {


                            System.out.println("states handler");

                            System.out.println(states.get(pressed).get(0).getClass());
//                            if(states.get(pressed) instanceof ProgramState) {
//                                heap = states.get
//                            }
//                            if (!states.get(states.size()-2).get(pressed).getHeap().toString().equals(heap.toString()))
//
//                            {
//                                heap = states.get(states.size()-2).get(pressed).getHeap();
//                                repopulateHeapTable();
//                            }

                            for(int k = 0; k < states.get(pressed).size(); k++) {
                                if(!heap.toString().equals(states.get(pressed).get(k).getHeap().toString())){
                                    heap = states.get(pressed).get(k).getHeap();
                                    repopulateHeapTable();
                                }
                                if(!out.toString().equals(states.get(pressed).get(k).getList().toString())){
                                    out = states.get(pressed).get(k).getList();
                                    repopulateOutList();
                                }
                                if(!symbolTables.get(states.get(pressed).get(k).getID()).toString().equals(states.get(pressed).get(k).getSymbolT().toString()))
                                {
//                                    symbolTables.put(states.get(pressed).get(k).getID(), states.get(pressed).get(k).getSymbolT());
                                    repopulateSymbolTable(states.get(pressed).get(k).getSymbolT());
                                }
                                if(!execStackMap.get(states.get(pressed).get(k).getID()).equals(states.get(pressed).get(k).getExecStack().toString())){
                                    repopulateExeStackList(states.get(pressed).get(k).getExecStack());
                                }
                            }
                            pressed ++;
                        }
                        catch (Exception e){
                            new Alert(Alert.AlertType.ERROR, "No more steps").showAndWait();
                        }


                    }
                });

                Label label1 = new Label("Number of ProgramStates:");
                TextField textField = new TextField (Integer.toString(numberOfProgramStates));
                HBox hb = new HBox();
                hb.getChildren().addAll(label1, textField, table, ouList, fileList, idList, symbolTable, exeStack);
                hb.setSpacing(10);

                VBox secondaryLayout = new VBox();
                secondaryLayout.getChildren().addAll(hb, show,runOneStep);
                Scene secondScene = new Scene(secondaryLayout, 1000, 500);
                Stage newWindow = new Stage();
                newWindow.setTitle("Program Execution");
                newWindow.setScene(secondScene);

                System.out.println(listView.getSelectionModel().getSelectedIndex());
                newWindow.show();

            }
        });
        layout.getChildren().addAll(listView, execute);



        window.setTitle("Toy Language GUI");
        Scene scene = new Scene(layout, 600,600);
        window.setScene(scene);

        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
